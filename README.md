# Single Page Application + Dark Mode (HTML, CSS, JAVASCRIPT)
## Execute the next commands in a terminal
> 1. git clone https://gitlab.com/ejorge607/singlepageapplication_and_darkmode.git
> 2. cd singlepageapplication_and_darkmode
> 3. npx webpack-dev-server (development mode)
> 4. npx webpack (production mode)


## Screenshots of the Pages
>### Home Page - Dark and Ligth Mode
>>![Home Page](src/assets/images-markdown/home.png "Home Page")
>>![Home Page](src/assets/images-markdown/home-dark_mode.png "Home Page Dark Mode")

>### Home Page - Generate Password - Dark and Ligth Mode
>>#### Press the generate password Button for obtain a random password
>>![Generate Password](src/assets/images-markdown/generate-pass.png "Generate Password")
>>![Generate Password](src/assets/images-markdown/generate-pass-dark.png "Generate Password Dark Mode")
>>#### After of generate a random password click the copy button
>>![Success Alert](src/assets/images-markdown/generate-pass-success.png "Success Alert")
>>![Success Alert](src/assets/images-markdown/success-alert.png "Success Alert Dark Mode")
>>#### No copy random password
>>![No Password](src/assets/images-markdown/generate-pass-failed.png "No Password")
>>![No Password](src/assets/images-markdown/fail-alert.png "No Password Dark Mode")

>### Gallery Page - Dark and Ligth Mode
>>#### Simple gallery design
>>![Gallery Page](src/assets/images-markdown/gallery.png "Gallery Page")
>>![Gallery Page](src/assets/images-markdown/gallery-dark_mode.png "Gallery Page Dark Mode")

>### Posts Page - Dark and Ligth Mode
>> #### Post data from [JSONPlaceholder](https://jsonplaceholder.typicode.com/)
>>![Posts Page](src/assets/images-markdown/post.png "Posts Page")
>>![Posts Page](src/assets/images-markdown/post-dark_mode.png "Posts Page Dark Mode")

>### Not Found Page - Dark and Ligth Mode
>>![Not Found Page](src/assets/images-markdown/404.png "Not Found Page")
>>![Not Found Page](src/assets/images-markdown/page_not_found-dark_mode.png "Not Found Page Dark Mode")


## The single page application is free, anyone can download and modify it
>### Author: Ing. Everardo Jorge
>>![Home Page](src/assets/img/ever_logo.png "Philadelphia's Magic Gardens")

