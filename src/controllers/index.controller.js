import Home from "./home.controller";
import Gallery from "./gallery.controller";
import Posts from "./posts.controller";
import notFound from "./404.controller";

const pages = {
    home: Home,
    gallery: Gallery,
    notFound: notFound,
    posts: Posts
};


export { pages };