import view from "../views/gallery.html";

export default async() => {
    const divElement = document.createElement('div');
    divElement.innerHTML = view;

    return divElement;
};