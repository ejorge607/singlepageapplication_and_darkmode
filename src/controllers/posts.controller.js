import view from "../views/posts.html";

const getPosts = async() => {
    const response = await fetch('https://jsonplaceholder.typicode.com/posts');
    return await response.json();
};

export default async() => {
    const divElement = document.createElement('div');
    divElement.innerHTML = view;

    const cardPosts = divElement.querySelector('#custom_card');

    let posts = await getPosts();
    posts.forEach(post => {
        cardPosts.innerHTML += `
        <div class="col-sm-6">
          <div class="card" id="simple_card">
            <div class="card-body">
              <h5 class="card-title">${post.title}</h5>
              <p class="card-text">${post.body}</p>
              <a href="#" class="btn btn-primary">${post.id}</a>
            </div>
          </div>
        </div>
        `;
    });
    return divElement;
};