import "bootstrap/dist/css/bootstrap.min.css";
import "../css/main.scss";
import "../assets/img/1.jpg";
import "../assets/img/2.jpg";
import "../assets/img/3.jpg";
import "../assets/img/4.jpg";
import "../assets/img/5.jpg";
import "../assets/img/6.jpg";
import "../assets/img/p404.png";
import "../assets/img/ever_logo.png";
import { router } from "../router/index.routes";

router(window.location.hash);
window.addEventListener('hashchange', () => {
    router(window.location.hash);
});

var container_box = document.getElementById('container-box');
window.onmousemove = function(e) {
    var x = -e.clientX / 5,
        y = -e.clientY / 5;

    container_box.style.backgroundPositionX = x + 'px';
    container_box.style.backgroundPositionY = y + 'px';
}



/**
 *
 * DARK MODE SETTINGS
 *
 */

const btnSwitch = document.querySelector('#switch');

btnSwitch.addEventListener('click', () => {
    document.body.classList.toggle('dark');
    btnSwitch.classList.toggle('active');

    /**
     *
     * SAVE DARK MODE SETINGS ON LOCALSTORAGE
     *
     */

    if (document.body.classList.contains('dark')) {
        localStorage.setItem('dark-mode', 'true');
    } else {
        localStorage.setItem('dark-mode', 'false');
    }
});


/**
 *
 * GET CURRENT MODE
 *
 */

if (localStorage.getItem('dark-mode') === 'true') {
    document.body.classList.add('dark');
    btnSwitch.classList.add('active');
} else {
    document.body.classList.remove('dark');
    btnSwitch.classList.remove('active');
}

/**
 *
 * END DARK MODE SETTINGS
 *
 */